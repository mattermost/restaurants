import traceback
from datetime import date, datetime, timedelta

import os
import requests
import sys


try:
    # mattermost webhook url
    MMHOOK = os.environ['MATTERMOST_HOOK']
    # bot owner for errors
    OWNER = os.environ['BOT_OWNER']
    # mattermost channel
    CHANNEL = os.environ['MATTERMOST_CHANNEL']
    # https://mynovae.ch key to access CERN restaurants
    NOVAE_KEY = os.environ['NOVAE_KEY']
except KeyError as exc:
    print(f'Required environment variable not set: {exc}')
    sys.exit(2)

# colors for the different restaurants
COLORS = {1: '#22dd77', 2: '#1188ee', 3: '#aa11ee'}

# CERN restaurant ids
RESTAURANTS = {
    1: '13-restaurant-r1',
    2: '21-restaurant-r2',
    3: '33-restaurant-r3',
}


def get_dishes(menus):
    dishes = []
    for item in menus:
        type_ = item['model']['title'].strip()
        title = (item['title'].get('fr') or item['title'].get('en')).strip().replace('\n', ' ').strip()
        dishes.append((type_, title))
    return sorted(dishes)


def mattermost_post_menus(day, menus):
    day = day.strftime('%A, %d %B %Y')
    fallback = []
    for num, menu in menus.items():
        if not menu:
            continue
        fallback.append(f'Dishes in R{num}:')
        for type_, name in menu:
            fallback.append(f'{type_}: {name}')
        fallback.append('')
    payload = {
        'channel': CHANNEL,
        'username': 'CERN Restaurants',
        'text': f'#### Menus for {day}',
        'attachments': [{
            'title': f'Restaurant {num}',
            'fallback': '\n'.join(fallback),
            'text': '' if menu else 'No menu available',
            'fields': [{'title': type_, 'value': dish, 'short': True} for type_, dish in menu] if menu else [],
            'color': COLORS[num],
        } for num, menu in menus.items()]
    }
    requests.post(MMHOOK, json=payload).raise_for_status()


def mattermost_post_error(title, message=None):
    if message is None:
        message = traceback.format_exc().strip()
    payload = {
        'channel': f'@{OWNER}',
        'username': 'CERN Restaurants',
        'attachments': [{
            'title': title,
            'fallback': message,
            'text': f'```\n{message}\n```',
            'color': '#FF5000'
        }]
    }
    requests.post(MMHOOK, json=payload).raise_for_status()


def get_data(restaurant_num: int, day_s: str):
    restaurant_id = RESTAURANTS[restaurant_num]
    url = f'https://api.mynovae.ch/en/api/v2/salepoints/{restaurant_id}/menus/week/{day_s}'

    try:
        response = requests.get(url, headers={'Novae-Codes': NOVAE_KEY})
        response.raise_for_status()
        data = response.json()
    except Exception:
        mattermost_post_error('Could not fetch menus')
        return

    return [entry for entry in data if entry['date'] == day_s]


def main():
    day = date.today()
    if datetime.now().hour > 14:
        day += timedelta(days=1)
    day_s = day.strftime('%Y-%m-%d')
    menus = {}
    for restaurant_num in RESTAURANTS:
        data = get_data(restaurant_num, day_s)
        try:
            menus[restaurant_num] = get_dishes(data)
        except Exception:
            mattermost_post_error(f'Could not extract dishes for R{restaurant_num}')
            continue

    # send errors if we don't have data for the 3 CERN restaurants
    if set(menus) != {1, 2, 3}:
        mattermost_post_error('Sanity check failed', f'Invalid restaurant list: {set(menus)}')
        # XXX: keep going, missing data is better than no data, especially until we know
        #      how they expose days where the restaurant is closed.
        if not menus:
            sys.exit(1)

    # send the menus to the channel
    try:
        mattermost_post_menus(day, menus)
        sys.exit(0)
    except Exception:
        mattermost_post_error('Could not post menus')
        sys.exit(1)


if __name__ == '__main__':
    main()
